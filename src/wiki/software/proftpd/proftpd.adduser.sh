#!/usr/bin/env bash
set -euo pipefail
#
# Copyright (c) 2018 Feral Hosting. This content may not be used elsewhere without explicit permission from Feral Hosting.
#
# This script can be used to create ProFTPd jails and users

# Functions

proftpdMenu ()
{
    echo -e "\033[36m""ProFTPd""\e[0m"
    if [[ ! -f ~/proftpd/etc/proftpd.conf ]] # check that user has (likely) installed configured ProFTPd
    then
        echo -e "You haven't fully installed ProFTPd yet - please follow the guide: \nhttps://www.feralhosting.com/wiki/software/proftpd"
        echo "Quitting the script..."
        echo
        break
    else
        echo "1 Create Jail"
        echo "2 List Jails"
        echo "3 Create User"
        echo "4 Remove User"
        echo "q Quit the script"
    fi
}

listJails ()
{
    if [[ ! -f ~/proftpd/etc/jails ]]
    then
        echo "You either have no created jails or you've removed the jails file."
    else
        echo "Here are your jails:"
        cat ~/proftpd/etc/jails
        echo
    fi
}

while [[ 1 ]]
do
    echo
    proftpdMenu
    echo
    read -ep "Enter the number of the option you want: " CHOICE
    echo
    case "$CHOICE" in
        "1") # create jail
            echo "A jail is the directory (and its subdirectories) that a user is restricted to."
            read -ep "Which directory do you want to be the jail (e.g. private/rtorrent/data)? " JAIL
            echo "<Directory $HOME/$JAIL>" >> ~/proftpd/etc/proftpd.conf
            echo
            read -ep "Do you want users to be able to write (and so DELETE) data from the jail? [y] yes or [n] no: " LIMITS
            if [[ $LIMITS =~ ^[Yy]$ ]]; then # if yes add stor, dele and write
                echo "<Limit STAT LSTAT DIRS READ STOR DELE WRITE>" >> ~/proftpd/etc/proftpd.conf
            else
                echo "<Limit STAT LSTAT DIRS READ>" >> ~/proftpd/etc/proftpd.conf
            fi
            echo
            read -ep "Give a (one word) name to your jail: " JAILNAME
            echo -e "# $JAILNAME\nAllowUser $(whoami)\nDenyAll\n</Limit>\n</Directory>\n\n" >> ~/proftpd/etc/proftpd.conf
            changes=1
            echo "$JAILNAME $HOME/$JAIL" >> ~/proftpd/etc/jails
            echo
            ;;
        "2") # list jails
            listJails
            sleep 3
            ;;
        "3") # create user
            listJails
            idcount=5001
            exec 6<&0
            exec 0<"$HOME/proftpd/etc/ftpd.passwd"
            while read line1
            do
                foundid="$(echo $line1 | grep -o $idcount || true)"
                if [ -n "$foundid" ]
                    then
                        idcount="$(expr $idcount + 1)"
                fi
            done
            exec 0<&6
            echo "Using $idcount as user and group ID"
            echo
            read -ep "Provide a username for your custom user: " USER
            echo
            read -ep "Which jail do you want to use (provide jail name)? " JAILSEARCH
            if grep -q "^\b$JAILSEARCH\b" ~/proftpd/etc/jails;
            then
                JAIL=$(awk -v search="$JAILSEARCH" '$1 == search {print $2}' ~/proftpd/etc/jails)
                ~/proftpd/bin/ftpasswd --passwd --name="$USER" --file="$HOME/proftpd/etc/ftpd.passwd" --uid="$idcount" --gid="$idcount" --home="$JAIL" --shell="/bin/false"
                ~/proftpd/bin/ftpasswd --group --name="$USER" --file="$HOME/proftpd/etc/ftpd.group" --gid="$idcount" --member="$USER"
                sed -i "s/# $JAILSEARCH$/# $JAILSEARCH\nAllowUser $USER/" ~/proftpd/etc/proftpd.conf
                changes=1
                echo $USER >> ~/proftpd/etc/users
            else
                echo "There's no jail with that name in your jails file."
            fi
            ;;
        "4") # remove user
            if [[ ! -f ~/proftpd/etc/users ]]
            then
                echo "You either have no created users or you've removed the users file."
            elif [[ ! -s ~/proftpd/etc/users ]]
            then
                echo "All users have already been removed."
            else
                echo "Here are the saved users:"
                cat ~/proftpd/etc/users
                echo
                read -ep "Enter the user you wish to remove from all jails: " DELUSER
                echo
                if grep -q "^\b$DELUSER\b" ~/proftpd/etc/users;
                then
                    sed -i "/AllowUser $DELUSER$/d" ~/proftpd/etc/proftpd.conf
                    sed -i "/^$DELUSER:/d" ~/proftpd/etc/ftpd.passwd
                    sed -i "/^$DELUSER:/d" ~/proftpd/etc/ftpd.group
                    sed -i "/^$DELUSER$/d" ~/proftpd/etc/users
                    changes=1
                    echo "User removed from all jails."
                else
                    echo "There's no user with that name in your users file."
                fi
            fi
            ;;
        "q") # quit the script entirely
            if changes=1;
            then
                echo "Changes have been made which require ProFTPd to be restarted - do so as per the guide:"
                echo "https://www.feralhosting.com/wiki/software/proftpd#start"
                exit
            else
                exit
            fi
            ;;
    esac
done